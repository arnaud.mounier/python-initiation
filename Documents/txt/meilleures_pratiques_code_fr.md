# Bonnes pratiques pour la production de code

1. **Écrire des programmes à destination d'êtres humains et non à des ordinateurs**
	* Un programme ne doit pas exiger pour être compris d'avoir en tête plus d'une poignée d'éléments
	* Choisir des noms cohérents, clivants et explicites
	* Choisir un formatage et un style de code constant et cohérent
2. **Laisser l'ordinateur travailler seul**
	* Laisser à l'ordinateur les tâches répétitives
	* Faire un historique des commandes en vue de leur réutilisation
	* Utiliser un outil spécifique pour concevoir et exécuter les workflows
3. **Faire des changements incrémentals**
	* Avancer par petits pas avec des tests fréquents tout en rectifiant les objectifs si nécessaire
	* Utiliser un système de contrôle de version
	* Gérer tous vos fichiers (code compris) avec un système de contrôle de gestion
4. **Ne faites aucune répétition**
	* Chaque information doit avoir une seule représention faisant autorité dans tout le système
	* Modulariser votre code plutôt que faire du copier-coller
	* Réutiliser votre code plutôt que le réécrire
5. **Prévoir les erreurs**
	* Ajouter des opérations de vérification
	* Utiliser des bibliothèques de test relatives aux langages utilisés
	* Considérer les bugs comme des opportunités de test
	* Utiliser un débuggeur symbolique
6. **Optimiser le programme seulement après une preuve de fonctionnement correct**
	* Utiliser un logiciel de profilage pour identifier les points critiques
	* Privilégier les langages de plus haut niveau d'abstraction (le plus proche du langage naturel ndt)
7. **La documentation doit détailler les intentions et les buts mais pas les recettes de cuisine** 
	* La documentation détaille et relie les buts mais dissimule des moyens (elle doit dire pourquoi mais pas comment ndt)
	* Remanier le code de préférence pour le rendre plus explicite
	* Inclure la documentation dans le code
8. **Collaborer**
	* Reviser son code avant de le fusionner (Use pre-merge code reviews)
	* Utiliser la programmation en binôme pour initier un nouveau rapidement ou pour faire face à un problème particulièrement difficile
	* Utiliser un outil de suivi
